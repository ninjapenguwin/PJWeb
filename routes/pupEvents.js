const { PupEvent, validate } = require('../models/pupEvent');
const { Puppy } = require('../models/puppy');

const auth = require('../middleware/auth');
const validateObject = require('../middleware/validate');
const validateObjectId = require('../middleware/validateObjectId');

const express = require('express');
const router = express.Router();

const _ = require('lodash');


// Add pupEvent
router.post('/', [auth, validateObject(validate)], async (req, res) => {
    const puppy = await Puppy.findById(req.body.puppyId);
    if (!puppy) return res.status(404).send('The puppy with given ID does not exist.');

    if (!_.find(puppy.ownerId, function (ch) { return ch == req.user._id; })) {
        return res.status(403).send('Access denied.');
    }

    let newPupEvent = new PupEvent({
        date: req.body.date,
        pooped: req.body.pooped,
        peed: req.body.peed,
        isAccident: req.body.isAccident,
        comments: req.body.comments,
        puppyId: req.body.puppyId
    });
    pupEvent = await newPupEvent.save();

    res.send(pupEvent);
});

// Delete pupEvent
router.delete('/:id', [auth, validateObjectId], async (req, res) => {
    const pupEvent = await PupEvent.findById(req.params.id);
    if (!pupEvent) return res.status(404).send('PupEvent with given ID does not exist.')

    const puppy = await Puppy.findById(pupEvent.puppyId);
    if (!puppy) return res.status(404).send('The puppy with given ID does not exist.');

    if (!_.find(puppy.ownerId, function (ch) { return ch == req.user._id; })) {
        return res.status(403).send('Access denied.');
    }

    await PupEvent.findOneAndDelete(req.params.id);

    res.send(pupEvent);
});

// Get pupEvent
router.get('/:id', [auth, validateObjectId], async (req, res) => {
    const pupEvent = await PupEvent.findById(req.params.id);
    if (!pupEvent) return res.status(404).send('PupEvent with given ID does not exist.');

    const puppy = await Puppy.findById(pupEvent.puppyId);
    if (!puppy) return res.status(404).send('The puppy with given ID does not exist.');

    if (!_.find(puppy.ownerId, function (ch) { return ch == req.user._id; })) {
        return res.status(403).send('Access denied.');
    }

    res.send(pupEvent);
});

// Update pupEvent
router.put('/:id', [auth, validateObjectId, validateObject(validate)], async (req, res) => {
    let pupEvent = await PupEvent.findById(req.params.id);
    if (!pupEvent) return res.status(404).send('PupEvent with given ID does not exist.');

    const puppy = await Puppy.findById(pupEvent.puppyId);
    if (!puppy) return res.status(404).send('The puppy with given ID does not exist.');

    if (!_.find(puppy.ownerId, function (ch) { return ch == req.user._id; })) {
        return res.status(403).send('Access denied.');
    }

    pupEvent = await PupEvent.findOneAndUpdate(req.params.id,
        { $set: req.body },
        { new: true });

    res.send(pupEvent);
});

module.exports = router;
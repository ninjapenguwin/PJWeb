const { User, validate } = require('../models/user');

const auth = require('../middleware/auth');
const validateObject = require('../middleware/validate');

const express = require('express');
const router = express.Router();

const _ = require('lodash');
const bcrypt = require('bcrypt');

// Get user
router.get('/me', auth, async (req, res) => {
    const user = await User.findById(req.user._id).select('-password');
    if (!user) return res.status(404).send('User not found.')
    res.send(user);
});

// Register new user
router.post('/', validateObject(validate), async (req, res) => {
    let user = await User.findOne({ email: req.body.email });
    if (user) return res.status(400).send('User already registered.');

    user = new User(_.pick(req.body, ['name', 'email', 'puppies', 'password']));
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);

    await user.save();

    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email']));
});

module.exports = router;
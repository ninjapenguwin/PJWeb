const { Puppy, validate } = require('../models/puppy');
const { PupEvent } = require('../models/pupEvent');

const auth = require('../middleware/auth');
const validateObject = require('../middleware/validate');
const validateObjectId = require('../middleware/validateObjectId');

const express = require('express');
const router = express.Router();

const _ = require('lodash');

// Get user's Puppies
router.get('/me', auth, async (req, res) => {
    const puppies = await Puppy.find({ ownerId: req.user._id });
    res.send(puppies);
});

// Add New Puppy
router.post('/', [auth, validateObject(validate)], async (req, res) => {
    let puppy = new Puppy({
        name: req.body.name,
        dob: req.body.dob,
        weight: req.body.weight,
        pupEvents: req.body.pupEvents,
        ownerId: req.user._id
    });
    puppy = await puppy.save();

    res.send(puppy);
});

// Delete Puppy
router.delete('/:id', [auth, validateObjectId], async (req, res) => {
    const puppy = await Puppy.findById(req.params.id);
    if (!puppy) return res.status(404).send('The puppy with given ID does not exist.');

    if (!_.find(puppy.ownerId, function (ch) { return ch == req.user._id; })) {
        return res.status(403).send('Access denied.');
    }

    await PupEvent.deleteMany({ puppyId: req.params.id });
    await Puppy.findOneAndDelete(req.params.id);

    res.send(puppy);
});

// Update Puppy
router.put('/:id', [auth, validateObjectId, validateObject(validate)], async (req, res) => {
    let puppy = await Puppy.findById(req.params.id);
    if (!puppy) return res.status(404).send('The puppy with given ID does not exist.');

    if (!_.find(puppy.ownerId, function (ch) { return ch == req.user._id; })) {
        return res.status(403).send('Access denied.');
    }

    puppy = await Puppy.findOneAndUpdate(req.params.id,
        { $set: req.body },
        { new: true });

    res.send(puppy);
});

// Get all pupEvent
router.get('/:id/pupEvents', [auth, validateObjectId], async (req, res) => {
    const pupEvents = await PupEvent.find({ puppyId: req.params.id });
    res.send(pupEvents);
})

module.exports = router;
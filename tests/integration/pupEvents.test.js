const { User } = require('../../models/user');
const { Puppy } = require('../../models/puppy');
const { PupEvent } = require('../../models/pupEvent');
const request = require('supertest');

describe('/api/pupEvents', () => {
    let server;

    beforeEach(() => {
        server = require('../../index');
    });
    afterEach(async () => {
        await User.deleteMany({});
        await Puppy.deleteMany({});
        await PupEvent.deleteMany({});
        await server.close();
    });

    describe('POST /', () => {
        let userId;
        let user;
        let puppy;
        let puppyId;
        let pupEvent;
        let token;

        const exec = () => {
            return request(server)
                .post('/api/pupEvents/')
                .set('x-auth-token', token)
                .send({
                    puppyId: puppyId
                });
        }

        beforeEach(async () => {
            user = new User({
                name: 'name1',
                email: 'email1',
                password: 'password1'
            });
            user = await user.save();
            userId = user._id;

            puppy = new Puppy({
                name: 'name1',
                ownerId: userId
            });
            puppy = await puppy.save();
            puppyId = puppy._id;

            pupEvent = new PupEvent({
                date: new Date(),
                puppyId: puppyId
            });
            await pupEvent.save();

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return pupEvent object if request is valid', async () => {
            const res = await exec();

            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('puppyId', puppyId.toHexString());
        });

        it('should return 400 if puppyId is not provided', async () => {
            puppyId = '';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 404 if puppy with given puppyId does not exist', async () => {
            await Puppy.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 403 if puppy.ownerId is different than userId', async () => {
            let user2 = new User({
                name: 'name2',
                email: 'email2@email.com',
                password: 'password2'
            });
            user2 = await user2.save();
            token = user2.generateAuthToken();

            const res = await exec();

            expect(res.status).toBe(403);
        });
    });

    describe('DELETE /id', () => {
        let userId;
        let user;
        let puppyId;
        let puppy;
        let pupEvent;
        let pupEventId;
        let token;

        const exec = () => {
            return request(server)
                .delete('/api/pupEvents/' + pupEventId)
                .set('x-auth-token', token)
                .send();
        }

        beforeEach(async () => {
            user = new User({
                name: 'name1',
                email: 'email1@email.com',
                password: 'password1'
            });
            user = await user.save();
            userId = user._id;

            puppy = new Puppy({
                name: 'name1',
                ownerId: userId
            });
            puppy = await puppy.save();
            puppyId = puppy._id;

            pupEvent = new PupEvent({
                date: new Date(),
                puppyId: puppyId
            });
            pupEvent = await pupEvent.save();
            pupEventId = pupEvent._id;

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return pupEvent object if request is valid', async () => {
            const res = await exec();

            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('puppyId', puppy._id.toHexString());
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 404 if puppy does not exist', async () => {
            await Puppy.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 404 if pupEvent with given Id does not exist', async () => {
            await PupEvent.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 403 if puppy.ownerId is different than userId', async () => {
            let user2 = new User({
                name: 'name2',
                email: 'email2@email.com',
                password: 'password2'
            });
            user2 = await user2.save();
            token = user2.generateAuthToken();

            const res = await exec();

            expect(res.status).toBe(403);
        });
    });

    describe('GET /id', () => {
        let userId;
        let user;
        let puppyId;
        let puppy;
        let pupEvent;
        let pupEventId;
        let token;

        const exec = () => {
            return request(server)
                .get('/api/pupEvents/' + pupEventId)
                .set('x-auth-token', token)
                .send();
        }

        beforeEach(async () => {
            user = new User({
                name: 'name1',
                email: 'email1@email.com',
                password: 'password1'
            });
            user = await user.save();
            userId = user._id;

            puppy = new Puppy({
                name: 'name1',
                ownerId: userId
            });
            puppy = await puppy.save();
            puppyId = puppy._id;

            pupEvent = new PupEvent({
                date: new Date(),
                puppyId: puppyId
            });
            pupEvent = await pupEvent.save();
            pupEventId = pupEvent._id;

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return pupEvent object if request is valid', async () => {
            const res = await exec();

            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('puppyId', puppy._id.toHexString());
        });

        it('should return 404 if pupEvent with given ID does not exist', async () => {
            await PupEvent.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 404 if puppy with give ID does not exist', async () => {
            await Puppy.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 403 if puppy.ownerId is different than userId', async () => {
            let user2 = new User({
                name: 'name2',
                email: 'email2@email.com',
                password: 'password2'
            });
            user2 = await user2.save();
            token = user2.generateAuthToken();

            const res = await exec();

            expect(res.status).toBe(403);
        });
    });

    describe('PUT /id', () => {
        let userId;
        let user;
        let puppyId;
        let puppy;
        let pupEvent;
        let pupEventId;
        let token;
        let isAccident

        const exec = () => {
            return request(server)
                .put('/api/pupEvents/' + pupEventId)
                .set('x-auth-token', token)
                .send({
                    isAccident: isAccident,
                    puppyId: puppyId
                });
        }

        beforeEach(async () => {
            user = new User({
                name: 'name1',
                email: 'email1@email.com',
                password: 'password1'
            });
            user = await user.save();
            userId = user._id;

            puppy = new Puppy({
                name: 'name1',
                ownerId: userId
            });
            puppy = await puppy.save();
            puppyId = puppy._id;

            isAccident = false
            pupEvent = new PupEvent({
                date: new Date(),
                puppyId: puppyId,
                isAccident: isAccident
            });
            pupEvent = await pupEvent.save();
            pupEventId = pupEvent._id;

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return pupEvent object if request is valid', async () => {
            isAccident = true;

            const res = await exec();

            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('puppyId', puppy._id.toHexString());
            expect(res.body).toHaveProperty('isAccident', isAccident);
        });

        it('should return 404 if pupEvent with given Id does not exist', async () => {
            await PupEvent.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 404 if puppy does not exist', async () => {
            await Puppy.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 400 if input is invalid', async () => {
            isAccident = 'a';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 403 if puppy.ownerId is different than userId', async () => {
            let user2 = new User({
                name: 'name2',
                email: 'email2@email.com',
                password: 'password2'
            });
            user2 = await user2.save();
            token = user2.generateAuthToken();

            const res = await exec();

            expect(res.status).toBe(403);
        });
    });
});
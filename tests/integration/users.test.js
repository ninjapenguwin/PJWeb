const { User } = require('../../models/user');
const request = require('supertest');
const mongoose = require('mongoose');
const _ = require('lodash');

describe('/api/user', () => {
    let server;

    beforeEach(() => {
        server = require('../../index');
    });
    afterEach(async () => {
        await User.deleteMany({});
        await server.close();
    });

    describe('GET /me', () => {
        let user;
        let userId;
        let token;

        const exec = () => {
            return request(server)
                .get('/api/users/me')
                .set('x-auth-token', token);
        }

        beforeEach(async () => {
            userId = mongoose.Types.ObjectId();
            user = new User({
                _id: userId,
                name: 'name1',
                email: 'email1',
                password: 'password1'
            });
            await user.save();

            token = user.generateAuthToken();
        });

        it('should return user object if a valid token is provided', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', user.name);
        });

        it('should return 404 if user with given id is not found', async () => {
            await User.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });
    });

    describe('POST /', () => {
        let user;

        const exec = () => {
            return request(server)
                .post('/api/users')
                .send(user)
        }

        beforeEach(() => {
            user = {
                name: 'name1',
                email: 'email1@email.com',
                password: 'password1'
            }
        });

        it('should return 400 if email is not provided', async () => {
            user.email = '';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        
        it('should return 400 if name is not provided', async () => {
            user.name = '';
            
            const res = await exec();

            expect(res.status).toBe(400);
        });

        
        it('should return 400 if password is not provided', async () => {
            user.password = '';
            
            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 400 if user already exists', async () => {
            existingUser = new User( {
                name: 'name1',
                email: 'email1@email.com',
                password: 'password1'
            });
            await existingUser.save();
            
            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return the user if input is valid', async () => {
            const res = await exec();

            expect(Object.keys(res.body)).toEqual(
                expect.arrayContaining(['name', 'email']));
        });

        it('should hash password if input is valid', async () => {
            await exec();

            const userInDb = await User.findOne({name: user.name});

            expect(userInDb.password).not.toBe(user.password);
        });

        it('should return token in response header if input is valid', async () => {
            const res = await exec();

            expect(res.header).toHaveProperty('x-auth-token');
        });
    });
});
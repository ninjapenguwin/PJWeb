const { User } = require('../../../models/user');
const request = require('supertest');
const mongoose = require('mongoose');

describe('auth middleware', () => {
    let server;

    beforeEach(() => { server = require('../../../index'); });
    afterEach(async () => {
        await User.deleteMany({});
        await server.close();
    });

    let token;
    let user;

    const exec = () => {
        return request(server)
            .get('/api/users/me')
            .set('x-auth-token', token)
    }

    beforeEach(async () => {
        user = new User({
            _id: mongoose.Types.ObjectId(),
            name: 'name1',
            email: 'email1',
            password: 'password1'
        });
        await user.save();

        token = user.generateAuthToken();
    });

    it('should return 401 if no token is provided', async () => {
        token = '';

        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 401 if token is invalid', async () => {
        token = 'a';

        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 200 if token is valid', async () => {
        const res = await exec();

        expect(res.status).toBe(200);
    });
});
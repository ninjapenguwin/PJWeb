const { User } = require('../../models/user');
const { Puppy } = require('../../models/puppy');
const {PupEvent } = require('../../models/pupEvent');
const request = require('supertest');
const mongoose = require('mongoose');

describe('/api/puppies', () => {
    let server;

    beforeEach(() => {
        server = require('../../index');
    });
    afterEach(async () => {
        await User.deleteMany({});
        await Puppy.deleteMany({});
        await PupEvent.deleteMany({});
        await server.close();
    });

    describe('GET /me', () => {
        let userId;
        let user;
        let puppy;
        let token;

        const exec = () => {
            return request(server)
                .get('/api/puppies/me')
                .set('x-auth-token', token)
                .send();
        }

        beforeEach(async () => {

            user = new User({
                name: 'name1',
                email: 'email1',
                password: 'password1'
            });
            await user.save();
            userId = user._id;


            puppy = new Puppy({
                name: 'name1',
                ownerId: userId
            });
            await puppy.save();

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return lists of users puppies if request is valid', async () => {
            const res = await exec();

            expect(res.body).toEqual(
                expect.arrayContaining([
                    expect.objectContaining({
                        name: puppy.name
                    })
                ])
            )
        });

        it('should return 400 if token is not provided', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 404 if user with given id is not found', async () => {
            await User.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });
    });

    describe('POST /', () => {
        let userId;
        let user;
        let puppyName;
        let token;

        const exec = () => {
            return request(server)
                .post('/api/puppies/')
                .set('x-auth-token', token)
                .send({
                    name: puppyName,
                    ownerId: userId
                });
        }

        beforeEach(async () => {
            userId = mongoose.Types.ObjectId();
            user = new User({
                _id: userId,
                name: 'name1',
                email: 'email1@email.com',
                password: 'password1'
            });
            await user.save();

            puppyName = 'name1';

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return puppy object if request is valid', async () => {
            const res = await exec();

            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', puppyName);
        });

        it('should return 400 if name is not provided', async () => {
            puppyName = '';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });
    });

    describe('DELETE /id', () => {
        let userId;
        let user;
        let puppyId;
        let token;

        const exec = () => {
            return request(server)
                .delete('/api/puppies/' + puppyId)
                .set('x-auth-token', token)
                .send();
        }

        beforeEach(async () => {
            userId = mongoose.Types.ObjectId();
            user = new User({
                _id: userId,
                name: 'name1',
                email: 'email1@email.com',
                password: 'password1'
            });
            await user.save();

            puppyId = mongoose.Types.ObjectId().toHexString();
            puppy = new Puppy({
                _id: puppyId,
                name: 'name1',
                ownerId: userId
            });
            await puppy.save();

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return puppy object if request is valid', async () => {
            const res = await exec();

            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', puppy.name);
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 404 if puppy does not exist', async () => {
            await Puppy.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 403 if puppy.ownerId is different than userId', async () => {
            let user2 = new User({
                name: 'name2',
                email: 'email2@email.com',
                password: 'password2'
            });
            user2 = await user2.save();
            token = user2.generateAuthToken();

            const res = await exec();

            expect(res.status).toBe(403);
        });
    });

    describe('PUT /id', () => {
        let userId;
        let user;
        let puppyId;
        let newName;
        let token;

        const exec = () => {
            return request(server)
                .put('/api/puppies/' + puppyId)
                .set('x-auth-token', token)
                .send({
                    name: newName
                });
        }

        beforeEach(async () => {
            userId = mongoose.Types.ObjectId();
            user = new User({
                _id: userId,
                name: 'name1',
                email: 'email1@email.com',
                password: 'password1'
            });
            await user.save();

            puppyId = mongoose.Types.ObjectId();
            puppy = new Puppy({
                _id: puppyId,
                name: 'name1',
                ownerId: userId
            });
            await puppy.save();

            newName = 'newName1'

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return updated puppy object if request is valid', async () => {
            const res = await exec();

            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', newName);
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 404 if puppy does not exist', async () => {
            await Puppy.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });

        it('should return 403 if puppy.ownerId is different than userId', async () => {
            let user2 = new User({
                name: 'name2',
                email: 'email2@email.com',
                password: 'password2'
            });
            user2 = await user2.save();
            token = user2.generateAuthToken();

            const res = await exec();

            expect(res.status).toBe(403);
        });
    });

    describe('GET /id/pupEvents', () => {
        let userId;
        let user;
        let puppy;
        let puppyId;
        let pupEvent;
        let token;

        const exec = () => {
            return request(server)
                .get('/api/puppies/' + puppyId + '/pupEvents')
                .set('x-auth-token', token)
                .send();
        }

        beforeEach(async () => {

            user = new User({
                name: 'name1',
                email: 'email1',
                password: 'password1'
            });
            await user.save();
            userId = user._id;


            puppy = new Puppy({
                name: 'name1',
                ownerId: userId
            });
            puppy = await puppy.save();
            puppyId = puppy._id;

            pupEvent = new PupEvent({
                date: new Date(),
                puppyId: puppyId
            });
            await pupEvent.save();

            token = user.generateAuthToken();
        });

        it('should return 200 if request is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        });

        it('should return lists of users puppies if request is valid', async () => {
            const res = await exec();

            expect(res.body).toEqual(
                expect.arrayContaining([
                    expect.objectContaining({
                        puppyId: puppyId.toHexString()
                    })
                ])
            )
        });

        it('should return 400 if token is not provided', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 404 if user with given id is not found', async () => {
            await User.deleteMany({});

            const res = await exec();

            expect(res.status).toBe(404);
        });
    });
});
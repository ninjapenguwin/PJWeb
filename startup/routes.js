const express = require('express');
const puppies = require('../routes/puppies');
const pupEvents = require('../routes/pupEvents');
const users = require('../routes/users');
const auth = require('../routes/auth');
const error = require('../middleware/error');

module.exports = function(app) {
    app.use(express.json());
    app.use('/api/puppies', puppies);
    app.use('/api/pupEvents', pupEvents);
    app.use('/api/users', users);
    app.use('/api/auth', auth);
    app.use(error);
}
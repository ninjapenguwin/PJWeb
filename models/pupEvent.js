const mongoose = require('mongoose');
const Joi = require('joi');
const Puppy = require('./puppy');

const pupEventSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true,
        default: Date.now
    },
    pooped: {
        type: Boolean,
        required: true,
        default: false
    },
    peed: {
        type: Boolean,
        required: true,
        default: false
    },
    isAccident: {
        type: Boolean,
        required: true,
        default: false
    },
    comments: {
        type: String,
        maxlength: 255
    },
    puppyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Puppies',
        required: true
    }
});

const PupEvent = mongoose.model('PupEvents', pupEventSchema);

function validatePupEvent(pupEvent) {
    const schema = {
        date: Joi.date(),
        pooped: Joi.boolean(),
        peed: Joi.boolean(),
        isAccident: Joi.boolean(),
        comments: Joi.string().max(255).allow(''),
        puppyId: Joi.objectId().required()
    }

    return Joi.validate(pupEvent, schema);
};

exports.PupEvent = PupEvent;
exports.validate = validatePupEvent;
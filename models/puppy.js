const mongoose = require('mongoose');
const Joi = require('joi');
//const User = require('./user');

const puppySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        minlength: 2,
        maxlength: 255
    },
    dob: Date,
    weight: {
        type: Number,
        min: 0
    },
    ownerId: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    }]
});

const Puppy = mongoose.model('Puppies', puppySchema);

function validatePuppy(puppy) {
    const schema = {
        name: Joi.string().min(2).max(50).required(),
        dob: Joi.date(),
        weight: Joi.number().min(0),
        ownerId: Joi.objectId()
    };
    
    return Joi.validate(puppy, schema);
}

exports.Puppy = Puppy;
exports.validate = validatePuppy;

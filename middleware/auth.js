const { User } = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = async function (req, res, next) {
    const token = req.header('x-auth-token');
    if (!token) return res.status(401).send('Access denied. No token provided.');

    try {
        const decoded = jwt.verify(token, config.get('jwtPrivateKey'));
        req.user = decoded;        
    }
    catch (ex) {
        return res.status(401).send('Invalid token.');
    }

    if (!req.user) return res.status(401).send('Invalid token');

    const userInDb = await User.findById({ _id: req.user._id });
    if (!userInDb) return res.status(404).send('User with given Id does not exist.');  
    
    next();
}